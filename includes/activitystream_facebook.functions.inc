<?php

/**
 * @file
 * Import Facebook feeds into a user's Activity Stream.
 */

/**
 * Get an access token.
 */
function _activitystream_facebook_pull_access_token($app_id, $app_secret) {

  $endpoint = ACTIVITYSTREAM_FACEBOOK_GRAPH_URL . 'oauth/access_token';
  $token_params = array(
    'type' => 'client_cred',
    'client_id' => $app_id,
    'client_secret' => $app_secret,
  );

  $data = array(
    'data' => http_build_query($token_params, NULL, '&'),
    'method' => 'POST',
  );
  $request = drupal_http_request($endpoint, $data);

  if ($request->code != 200) {
    $message = 'code: ' . $request->code . ' message: ' . $request->status_message;
    watchdog('facebook_request_token', $message, array(), WATCHDOG_WARNING);
    return FALSE;
  }

  return $request->data;
}

/**
 * Pull data from a facebook feed.
 */
function _activitystream_facebook_pull_items($type, $token, $options = array()) {

  $qs = http_build_query($options, '', '&');
  $url = ACTIVITYSTREAM_FACEBOOK_GRAPH_URL . "%s?%s&%s";
  $full_url = sprintf($url, $type, $token, $qs);

  $facebook_data = _activitystream_facebook_request_data($full_url);

  $items = array();
  $body = array();
  foreach ($facebook_data as $item) {

    if ($item->type == 'photo') {
      $link = $item->link;
      $body = isset($item->message) ? $item->message : $item->caption;
    }
    else {
      $link = ACTIVITYSTREAM_FACEBOOK_URL . $item->id;
      $body = $item->message;
    }

    $body = preg_replace_callback('/(#)([\w\dçãõéêàáâóôíú\'\"]+)/', '_activitystream_facebook_url_lowercase', $body);

    // Get the fields picture and link from user profile.
    $fields = _activitystream_facebook_request_graph(array('ids' => $item->from->id, 'fields' => 'picture, link'));

    // Picture url.
    $item->from->picture = $fields->{$item->from->id}->picture->data->url;

    // Link profile.
    $item->from->link = $fields->{$item->from->id}->link;
    $items[] = array(
      'title'     => 'Facebook: ' . $item->id,
      'body'      => _activitystream_facebook_remove_emoji($body),
      'link'      => $link,
      'guid'      => $item->id,
      'raw'       => json_encode($item),
      'timestamp' => strtotime($item->created_time),
    );
  }

  return $items;
}

/**
 * Pull data from a facebook use Graph API.
 *
 * @link https://developers.facebook.com/docs/graph-api/using-graph-api/. @endlink
 */
function _activitystream_facebook_request_graph($options) {

  $qs = http_build_query($options, '', '&');
  $url = ACTIVITYSTREAM_FACEBOOK_GRAPH_URL . "?%s";
  $full_url = sprintf($url, $qs);

  return _activitystream_facebook_request_data($full_url);
}

/**
 * Get the data stream on facebook.
 */
function _activitystream_facebook_request_data($url) {

  $request = drupal_http_request($url);
  $data = json_decode($request->data);

  if ($request->code != 200) {
    $message = 'code: ' . $request->code . ' message: ' . $request->status_message;
    watchdog('facebook_request_token', $message, array(), WATCHDOG_WARNING);
    return FALSE;
  }

  return isset($data->data) ? $data->data : $data;
}

/**
 * Get the data stream on facebook.
 */
function _activitystream_facebook_url_lowercase($match) {

  $link = ACTIVITYSTREAM_FACEBOOK_URL . 'hashtag/';

  // $match[0] == #hashtag; $match[1] == #;  $match[2] == hashtag.
  return l($match[0], $link . strtolower($match[2]), array('attributes' => array('target' => '_blank')));
}

/**
 * Remove the emoji.
 */
function _activitystream_facebook_remove_emoji($text) {

  return preg_replace('/([0-9|#][\x{20E3}])|[\x{00ae}|\x{00a9}|\x{203C}|\x{2047}|\x{2048}|\x{2049}|\x{3030}|\x{303D}|\x{2139}|\x{2122}|\x{3297}|\x{3299}][\x{FE00}-\x{FEFF}]?|[\x{2190}-\x{21FF}][\x{FE00}-\x{FEFF}]?|[\x{2300}-\x{23FF}][\x{FE00}-\x{FEFF}]?|[\x{2460}-\x{24FF}][\x{FE00}-\x{FEFF}]?|[\x{25A0}-\x{25FF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{FE00}-\x{FEFF}]?|[\x{2900}-\x{297F}][\x{FE00}-\x{FEFF}]?|[\x{2B00}-\x{2BF0}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F6FF}][\x{FE00}-\x{FEFF}]?/u', '', $text);
}

/**
 * Format date.
 *
 * If the difference between the $created_date and $current_date
 * hours are the hours will be returned. If less than one hour
 * will be returned minutes. If greater than one day be returned the
 * date of creation.
 *
 * @param object $created_date
 *   The DateTime object of creation stream.
 *
 * @return string
 *   The string with the date formatted.
 */
function _activitystream_facebook_diff_date($created_date) {

  $current_date = new DateTime();
  $diff_date = $created_date->diff($current_date);

  if ($diff_date->h >= 23 || $diff_date->d != 0) {
    return $created_date->format('d ' . t('\o\f') . ' F');
  }
  elseif ($diff_date->h == 0) {
    return $diff_date->i . 'min';
  }
  else {
    return $diff_date->h . 'h';
  }
}
