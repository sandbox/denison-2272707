<?php

/**
 * @file
 * Facebook template implementation for an Activity Stream item.
 *
 * Available variables:
 * - $profile: An array with information related to the user.
 * - $message: Shared message.
 * - $video_source: Url of video shared.
 * - $picture_source: An array with image tag and caption shared.
 * - $link: An array with link and description shared.
 */
?>

<div class="activitystream-wrapper">
  <div class="activitystream-facebook">
    <div class="activitystream-item-profile">
      <div class="image-profile">
        <?php print render($profile['image']); ?>
      </div>
      <div class="name-profile">
        <?php print render($profile['name']); ?>
      </div>
      <div class="created-date">
        <?php print render($profile['created_date']); ?>
      </div>
    </div>

    <div class="activitystream-item-content">
      <?php if(isset($message)) : ?>
        <div class="message">
          <?php print render($message); ?>
        </div>
      <?php endif;?>

      <?php if(isset($video_source)) : ?>
        <div class="video">
          <video controls>
            <source src=<?php print $video_source; ?>>
          </video>
        </div>
      <?php endif;?>

      <?php if(isset($picture_source)) : ?>
        <div class="picture-source">
          <?php print render($picture_source['img']); ?>
        </div>
        <span class="caption">
            <?php print $picture_source['caption']; ?>
        </span>
      <?php endif;?>

       <?php if(isset($link)) : ?>
        <div class="link">
          <a href=<?php print $link['url']; ?> target="_blank">
            <div class="image-link">
              <?php print render($link['picture']); ?>
            </div>

            <h3>
              <?php print $link['name']; ?>
            </h3>

            <span class="description-link">
              <?php print $link['description']; ?>
            </span>
          </a>
        </div>
      <?php endif;?>
    </div>
  </div>
</div>
